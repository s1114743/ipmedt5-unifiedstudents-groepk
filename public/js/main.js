const cornflakeToDb = document.getElementById("js--cornflakeToDb");
const cornflakeToDelete = document.getElementById("js--cornflakeToDelete");
const buttonPlan = document.getElementById("js--plan");
const cornflakeTitle = document.getElementById("js--cornflakeTitle");
const tempStatus = document.getElementById("js--temp");
const komStatus = document.getElementById("js--kom");
const compartmentLinksStatus = parseInt(
    document.getElementById("js--compartmentLinks").getAttribute("data-vol")
);
const compartmentRechtsStatus = parseInt(
    document.getElementById("js--compartmentRechts").getAttribute("data-vol")
);
const compartmentLinksButtons = document.getElementsByClassName(
    "compartment-links-buttons"
);
const compartmentRechtsButtons = document.getElementsByClassName(
    "compartment-rechts-buttons"
);

const compartmentLinks = document.getElementById("js--compartmentLinks");
const compartmentRechts = document.getElementById("js--compartmentRechts");
const compartmentStatusAlert = document.getElementById("js--addCornflakeAlert");
const formCompartmentEdit = document.getElementById("js--editCornflakeForm");
const BASE_URL = "http://127.0.0.1:8000/status";

window.onload = () => {
    pageChecker();
    // setValueToInsert();
    toggleCompartmentStatus();
    showEditFormInput("none");
    selectCornflakeDelete();
    changeStatus();
};

// Check op welke pagina je zit en voer hierdoor codes uit
pageChecker = () => {
    if (
        window.location.pathname == "/login" ||
        window.location.pathname == "/register"
    ) {
        document.getElementById("js--nav").className = "disable-nav";
        document.getElementsByTagName("BODY")[0].className =
            "background-cereal";
    }
    if (window.location.pathname == "/profiles") {
        console.log("/profiles");
    }
    if (window.location.pathname == "/") {
        document.getElementById("js--nav").className = "disable-nav";
        document.getElementsByTagName("BODY")[0].className =
            "background-cereal";
    }
};

// disabled de compartment selectie button op basis van de status
toggleCompartmentStatus = () => {
    console.log("Links: " + compartmentLinksStatus);
    console.log("Rechts: " + compartmentRechtsStatus);

    if (compartmentLinksStatus === 0) {
        console.log("Links is empty");
        for (let i = 0; i < compartmentLinksButtons.length; i++) {
            compartmentLinksButtons[i].disabled = true;
        }
    } else {
        compartmentLinks.disabled = true;
    }

    if (compartmentRechtsStatus === 0) {
        console.log("Rechts is empty");
        for (let i = 0; i < compartmentRechtsButtons.length; i++) {
            compartmentRechtsButtons[i].disabled = true;
        }
    } else compartmentRechts.disabled = true;

    if (compartmentLinksStatus && compartmentRechtsStatus === 1) {
        console.log("Both full");
        $(compartmentStatusAlert).css("display", "inline-flex");
    }
};

// selecteer de cornflake uit de db wanneer er op submit wordt geklikt.
selectCornflakeAdd = (id) => {
    cornflakeToDb.setAttribute("value", id);
};

showEditFormInput = (compartment) => {
    if (compartment === "links") {
        $(".compartment-links").css("display", "block");
        $(".compartment-rechts").css("display", "none");
    } else if (compartment === "rechts") {
        $(".compartment-links").css("display", "none");
        $(".compartment-rechts").css("display", "block");
    } else if (compartment === "none") {
        $(".compartment-links").css("display", "none");
        $(".compartment-rechts").css("display", "none");
    }
};

selectCornflakeDelete = () => {
    $("#js--compartmentLinksEdit").click(function () {
        // formCompartmentEdit.setAttribute("action", "/cornflake/remove/1");
        cornflakeToDelete.setAttribute("formaction", "/cornflake/remove/1");
        console.log("Links");
        showEditFormInput("links");
    });

    $("#js--compartmentRechtsEdit").click(function () {
        // formCompartmentEdit.setAttribute("formaction", "/cornflake/remove/2");
        cornflakeToDelete.setAttribute("formaction", "/cornflake/remove/2");
        console.log("Rechts");
        showEditFormInput("rechts");
    });
};

changeStatus = () => {
    fetch(BASE_URL)
        .then((data) => {
            return data.json();
        })
        .then((response) => {
            console.log(response[0].kom);
            tempStatus.innerHTML = response[0].kom;

            if (response[0].kom === "null") {
                komStatus.setAttribute("src", "/img/icons/checkmark.svg");
            } else {
                komStatus.setAttribute("src", "/img/icons/criss-cross.svg");
            }
        });
};
