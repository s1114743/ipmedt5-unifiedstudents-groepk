<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'LoginController@show');

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::post('/dashboard', 'HomeController@store');

# profiles
# logout, profiles
Route::get('/logout', 'PageController@logout');
Route::get('/status', 'PageController@status');
Route::get('/account','PageController@account')->middleware('auth');

Route::get('/profiles', 'PageController@profiles')->middleware('auth');

# Cornflakes toevoegen
Route::get('/cornflake/add', 'PageController@create');

Route::post('/cornflake/add', 'PageController@store');

Route::get('/cornflake/edit', 'PageController@update');

Route::patch('/cornflake/edit', 'PageController@update');


Route::post('/cornflake/remove/{id}', 'PageController@destroy');


Route::get('/led', 'LedController@aanuit');

Route::get('/led/links', 'LedController@cornflakeLinks');

Route::get('/led/rechts', 'LedController@cornflakeRechts');

// Queue 
Route::post('/queue/add', 'PlanningsController@store');
Route::get('/queue/remove/{id}', 'PlanningsController@destroy');

// DEV
Route::get('/dev', 'PageController@dev');

// STATUS
Route::get('/status', 'HomeController@status');