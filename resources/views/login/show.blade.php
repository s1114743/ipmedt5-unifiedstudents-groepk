@extends('layout')

@section('content')
    <h1>Welcome to the login section</h1>

    <form action="" method="post">
        <div class="loginSection">
            <div class="loginPart">
                <a class="loginName">Username:</a>
                <input type="text" name="username" class="loginInput">
            </div>
            <div class="loginPart">
                <a class="loginName">Password:</a>
                <input type="password" name="password" class="loginInput">
            </div>
            <div class="loginButtonDiv">
                <button type="submit" id="loginButton">Login</button>
            </div>
        </div>
    </form>
@endsection
