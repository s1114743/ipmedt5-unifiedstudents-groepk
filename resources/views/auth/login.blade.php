@extends('layouts.app')

@section('content')

<!-- <button><a href="/led" style="color: black">Led aan/uit</a></button>
<button><a href="/led/links" style="color: black">Led links</a></button>

<button><a href="/led/rechts" style="color: black">Led rechts</a></button> -->


<div class="container h-100 login">
    <div class="row h-100 justify-content-center align-items-center">
        <form class="col-md-4" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <img class="login--logo-image" src="{{ URL::asset('img/cereals.svg') }}" alt="">
                <a class="login--logo" href="#">my<span class="title--secondary">Flakes</span> </a>
            </div>
            <div class="form-group text-center font-italic login--quote">
                <p>Start je dag met een verse bak cornflakes</p>
            </div>
            <div class="form-group text-center">
                <hr class="login--hr">
            </div>
            <div class="form-group">
                <label for="email" class="col-form-label text-md-right login--label">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror login--input"
                    name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password" class="col-form-label text-md-right login--label">{{ __('Password') }}</label>
                <input id="password" type="password"
                    class="form-control @error('password') is-invalid @enderror login--input" name="password" required
                    autocomplete="current-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary w-100 login--button">{{ __('LOGIN') }}</button>
            </div>
            <div class="form-group text-center">
                <a href="{{'/register'}}">Nog geen account? Registeren </a>
            </div>
        </form>
    </div>
</div>
@endsection
</div>