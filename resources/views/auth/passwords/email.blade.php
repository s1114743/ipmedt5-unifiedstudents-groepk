@extends('layouts.app')
@section('navbar-title', 'Account')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if (session('status'))
            <div class="alert alert-success" role="alert">{{ session('status') }}</div>
            @endif

            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

            <div class="card">

                <h2 class="card-header">Wijzig wachtwoord</h2>

                <div class="card-body">
                    <p class="card-text">Voer het e-mailadres in dat gekoppeld aan uw account is. Hier wordt een e-mail
                        naar toe gestuurd met verdere instructies.</p>

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="row justify-content-center">
                            <div class="form-group">
                                <label for="email"
                                    class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email"
                                    class="card__input form-control @error('email') is-invalid @enderror" name="email"
                                    value="{{ old('email') }}" required autocomplete="email" autofocus>

                            </div>
                        </div>
                </div>
                <div class="card-footer">
                    <div class="row justify-content-center">
                        <button type="submit" class="card__button btn btn-primary">{{ __('Verstuur') }}</button>
                    </div>
                </div>

                </form>

            </div>


        </div>
    </div>
</div>
@endsection