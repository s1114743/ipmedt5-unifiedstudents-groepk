@extends('layouts.app')

@section('content')

<div class="container h-100 login">
  <div class="row h-100 justify-content-center align-items-center">
    <form class="col-md-4" method="POST" action="{{ route('register') }}">
      @csrf
      <div class="form-group">
        <a class="login--logo" href="#">my<span class="title--secondary">Flakes</span> </a>
      </div>
      <div class="form-group text-center font-italic login--quote">
        <p>Start je dag met een verse bak cornflakes</p>
      </div>
      <div class="form-group text-center">
        <hr class="login--hr">
      </div>
      <div class="form-group">
        <label for="name" class="col-form-label text-md-right login--label">{{ __('Name') }}</label>
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror login--input" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>
      <div class="form-group">
        <label for="email" class="col-form-label text-md-right login--label">{{ __('E-Mail Address') }}</label>
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror login--input" name="email" value="{{ old('email') }}" required autocomplete="email">
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>
      <div class="form-group">
        <label for="password" class="col-form-label text-md-right login--label">{{ __('Password') }}</label>
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror login--input" name="password" required autocomplete="new-password">
        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group">
        <label for="password-confirm" class="col-form-label text-md-right login--label">{{ __('Confirm Password') }}</label>
            <input id="password-confirm" type="password" class="form-control login--input" name="password_confirmation" required autocomplete="new-password">
      <div class="form-group text-center">
        <button type="submit" class="btn btn-primary w-100 login--button">{{ __('REGISTER') }}</button>
      </div>
      <div class="form-group text-center">
        <a href="{{'/login'}}">Al geregisteerd? Login </a>
      </div>
    </form>
  </div>
</div>
@endsection
</div>