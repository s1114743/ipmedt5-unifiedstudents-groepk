@extends('layouts.app')
@section('navbar-title', 'Account')
@section('content')

<div class="container">

    <!-- <div class="row">
        <div class="col-md-6">
            <h2>E-mailadres</h2>
        </div>
    </div> -->

    <!-- <div class="row justify-content-center">
        <div class="col-md-6">
            <p>{{{ Auth::user()->email }}}</p>
        </div>

        <div class="col-md-6">
            <a class="link-right" href="#">E-mailadres wijzigen</a>
        </div>
    </div> -->

    <hr>

    <div class="row">
        <div class="col-md-6">
            <h2>Wachtwoord</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <p>***********</p>
        </div>

        <div class="col-md-6">
            <a class="link-right" href="/password/reset">Wachtwoord wijzigen</a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <h2>Geschiedenis</h2>
        </div>
    </div>
    <table class="table text-white">
        <thead>
            <tr>
                <th scope="col">Cornflake</th>
                <th scope="col">Tijd</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($history as $queue)
            <tr>
                <!-- <th scope="row">1</th> -->
                <td>{{$queue->cornflake}}</td>
                <td>{{$queue->time}}</td>
                <!-- <td>@mdo</td> -->
            </tr>
            @endforeach
        </tbody>
    </table>



</div>

@endsection