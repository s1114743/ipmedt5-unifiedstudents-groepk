@extends('layouts.app')

@section('content')
<form action="/add" method="post">
    @csrf
    <input type="text" name="naam" id="">
    <input type="text" name="merk" id="">
    <select name="soort" id="">
        @foreach($soorten as $soort)
        <option value="{{$soort->soort}}">{{$soort->soort}}</option>
        @endforeach
    </select>
    <button type="submit" name="button">Add</button>
</form>
@endsection