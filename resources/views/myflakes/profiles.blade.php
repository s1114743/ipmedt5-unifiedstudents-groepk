@extends('layouts.app')
@section('navbar-title', 'Profiles')
@section('content')
<div class="">
    <div class="row">
      <div class="col-md-4">
        <div class="default-profile">
          <h1>Cocopops</h1>
          <button type="button" class="btn btn-primary">Plan</button>
          <button type="button" class="btn btn-bewerk">Bewerk</button>
        </div>
      </div>
      <div class="col-md-4">
        <div class="default-profile">
          <h1>Cruesli</h1>
          <button type="button" class="btn btn-primary">Plan</button>
          <button type="button" class="btn btn-bewerk">Bewerk</button>
        </div>
      </div>
      <div class="col-md-4">
        <div class="default-profile">
          <h1>Frosted flakes</h1>
          <button type="button" class="btn btn-primary">Plan</button>
          <button type="button" class="btn btn-bewerk">Bewerk</button>
        </div>
      </div>
    </div>
</div>
@endsection