@extends('layouts.app')
@section('navbar-title', 'Dashboard')
@section('content')

<div class="container">

    @if (session('status'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session('status') }}
            </div>

        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <h2 class="dashboard__title">Status</h2>
        </div>
    </div>

    <div class="row">

        <div class="col-md-6 dashboard__status__col">
            <p>Temperatuur melk:</p>
            <p class="dashboard__status__text--temperature"><span id="js--temp">{{$data['status']->temp }}</span>
                &#8451;
            </p>
        </div>

        <div class="col-md-6 dashboard__status__col">
            <p>Schaaltje geplaatst:</p>
            <img class="dashboard__status_icon--bowl" src="/img/icons/bowl.svg" alt="bowl">
            <img src="/img/icons/checkmark.svg" id="js--kom" class="dashboard__status_icon--checkmark" alt="checkmark">
        </div>

    </div>


    <hr>

    <div class="row">
        <div class="col-md-12">
            <h2 class="dashboard__title">Cornflakes</h2>
        </div>
    </div>


    <div class="row">

        <div class="col-md-4 compartment-links-body">
            <div class="default-profile">
                <h2 id="js--cornflakeTitle">{{$data['compartmentLinks']->naam }}</h2>
                <button type="button" class="btn btn-primary compartment-links-buttons" data-toggle="modal"
                    id="js--plan" data-target="#planner" onclick="selectCornflakeAdd(1)">Plan</button>
                <button type="button" class="btn btn-danger compartment-links-buttons" data-toggle="modal"
                    data-target="#cornflake-bewerken" id="js--compartmentLinksEdit">Edit</button>
            </div>
        </div>

        <div class="col-md-4 compartment-rechts-body">
            <div class="default-profile">
                <h2 id="js--cornflakeTitle">{{$data['compartmentRechts']-> naam }}</h2>
                <button type="button" class="btn btn-primary compartment-rechts-buttons" data-toggle="modal"
                    id="js--plan" data-target="#planner" onclick="selectCornflakeAdd(2)">Plan</button>
                <button type="button" class="btn btn-danger compartment-rechts-buttons" data-toggle="modal"
                    data-target="#cornflake-bewerken" id="js--compartmentRechtsEdit">Edit</button>
            </div>
        </div>


        <div class="col-md-4">
            <button class="dashboard__add-button" data-toggle="modal" data-target="#cornflake-toevoegen"></button>
        </div>

    </div>


    <hr>

    <div class="row">
        <div class="col-md-12">
            <h2 class="dashboard__title">Upcoming queue</h2>
        </div>
    </div>


    <!-- {{ $data['queue']}} -->
    <div class="row">
        @foreach ($data['queue'] as $queue)
        <div class="col-xs-6 queue__item">
            <div class="modal-queue">
                <div class="modal-queue__body">
                    <div class="modal-queue__content-left">
                        <div class="modal-queue__heading">
                            <h3 class="heading-3">{{$queue -> cornflake}}</h3>
                        </div>
                        <div class="modal-queue__icon">
                            <img src="{{ URL::asset('img/user.svg') }}" alt="User Icon" class="icon">
                            <span class="modal-queue__icon-info">{{$queue -> name}}</span>
                        </div>
                        <div class="modal-queue__icon">
                            <img src="{{ URL::asset('img/clock.svg') }}" alt="Time Icon" class="icon">
                            <span class="modal-queue__icon-info">{{$queue -> time}}</span>
                        </div>
                        <div class="modal-queue__content-right">
                            <div class="modal-queue__close">
                                <a href="/queue/remove/{{$queue -> id}}"><img src="{{ URL::asset('img/close.svg') }}"
                                        alt="close button" class="close"></a>
                            </div>

                            <div class="modal-queue__picture">
                                <img src="{{ URL::asset('img/cereal_box.svg') }}" alt="Cornflakes Icon" class="picture">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @endforeach
    </div>



    <!--pop up modal voor planning-->
    <div class="modal fade" id="planner" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Plan uw maaltijd</h5>
                </div>

                <div class="modal-body">
                    <div class="form-group">

                        <form class="" action="/queue/add" method="POST">
                            @csrf
                            <input type="hidden" id="js--cornflakeToDb" name="cornflake" value="">
                            <label for="">Datum</label>
                            <input class="form-control" type="date" name="datum" value="">
                            <br>
                            <label for="">Tijdstip</label>
                            <input class="form-control" type="time" name="tijdstip" value="">
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" name="plan">Plan</button>

                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                    <!-- <button type="submit" class="btn btn-primary" name="plan">Plan</button> -->
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- pop up modal voor planning => tweede modal voor toevoegen icoon -->
    <div class="modal fade" id="planIconModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="planIconModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="planIconModalCenterTitle">Kies een afbeelding of icoon</h5>
                </div>
                <div class="modal-body modal-icon">
                    <div class="modal-icon__content-up">
                        <div class="content-up__img content-up__img--1"></div>
                        <div class="content-up__img content-up__img--2"></div>
                        <div class="content-up__img content-up__img--3"></div>
                    </div>
                    <div class="modal-icon__content-down">
                        <div class="content-down__img content-down__img--1"></div>
                        <div class="content-down__img content-down__img--2"></div>
                        <div class="content-down__img content-down__img--3"></div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button type="submit" class="btn btn-primary" name="plan">Plan</button> -->
                </div>
            </div>
        </div>
    </div>

</div>


<!--pop up modal voor toevoegen cornflake-->
<div class="modal fade" id="cornflake-toevoegen" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Cornflake toevoegen</h5>
            </div>

            <div class="modal-body">
                <div class="form-group">

                    <form class="" action="/cornflake/add" method="post">
                        @csrf
                        <!-- {{ method_field('PUT') }} -->
                        <p class="alert alert-danger" id="js--addCornflakeAlert">Compartmenten zijn vol. Verwijder
                            andere cornflakes voor
                            je nieuwe kan toevoegen!
                        </p>
                        <label for="">Naam</label>
                        <input class="form-control" type="text" name="naam" id="" placeholder="Voorbeeld: Naturel">
                        <br>
                        <label for="">Merk</label>
                        <input class="form-control" type="text" name="merk" id="" placeholder="Voorbeeld: Kelloggs">
                        <label for="">Links</label>
                        <input class="form-control" type="radio" name="compartment" id="js--compartmentLinks"
                            value="links" data-vol="{{ $data['disableLinks'] }}">
                        <label for="">Rechts</label>
                        <input class="form-control" type="radio" name="compartment" id="js--compartmentRechts"
                            value="rechts" data-vol="{{ $data['disableRechts'] }}">
                </div>
            </div>

            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" name="button" class="btn btn-primary">Opslaan</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!--pop up modal voor bewerken cornflake-->
<div class="modal fade" id="cornflake-bewerken" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Cornflake bewerken</h5>
            </div>

            <div class="modal-body">
                <div class="form-group">

                    <form class="" id="js--editCornflakeForm" action="/cornflake/edit" method="POST">
                        <!-- {{method_field('PATCH')}} -->
                        @csrf
                        <!-- @method('PUT') -->
                        <input type="hidden" name="cornflake" value="">
                        <!-- <input type="hidden" id="js--cornflakeToDelete" name="cornflake" value=""> -->

                        <div class="compartment-links">
                            <label for="">Naam</label>
                            <input class="form-control" type="text" name="naam-links" id="js--cornflakeNaam"
                                value="{{$data['compartmentLinks']-> naam }}">
                            <br>
                            <label for="">Merk</label>
                            <input class="form-control" type="text" name="merk-links" id="js--cornflakeMerk"
                                value="{{$data['compartmentLinks']-> merk }}">
                        </div>

                        <div class="compartment-rechts">
                            <label for="">Naam</label>
                            <input class="form-control" type="text" name="naam-rechts" id="js--cornflakeNaam"
                                value="{{$data['compartmentRechts']-> naam }}">
                            <br>
                            <label for="">Merk</label>
                            <input class="form-control" type="text" name="merk-rechts" id="js--cornflakeMerk"
                                value="{{$data['compartmentRechts']-> merk }}">
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" formaction="" id="js--cornflakeToDelete" value="delete" name="button"
                    class="btn btn-danger">Delete</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" name="_method" value="PATCH" value="submit" name="button"
                    class="btn btn-primary">Opslaan</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection