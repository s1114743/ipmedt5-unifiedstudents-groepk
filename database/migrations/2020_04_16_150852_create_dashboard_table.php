<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDashboardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboard', function (Blueprint $table) {
            $table->date('datum')->nullable();
            $table->time('tijdstip')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->string('naam')->nullable();
            
            // $table->foreign('naam')->references('cornflakes')->on('naam');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboard');

        // Schema::table('dashboard', function(Blueprint $table){
        //     $table->dropForeign('dashboard_naam_foreign');
        // });
    }
}
