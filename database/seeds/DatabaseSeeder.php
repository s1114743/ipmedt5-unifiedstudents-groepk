<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CornflakesTableSeeder::class);
        $this->call(QueueTableSeeder::class);
        $this->call(LedTableSeeder::class);
        $this->call(CompartmentTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(QueueHistoryTableSeeder::class);
    }
}