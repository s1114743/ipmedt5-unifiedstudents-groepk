<?php

use Illuminate\Database\Seeder;

class QueueHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('queue_history')->insert([
            'cornflake' => 'whut',
            'compartment' => 'rechts',
            'name' => 'Vyash',
            'time' => '20:20 2020-06-24'
        ]);
    }
}