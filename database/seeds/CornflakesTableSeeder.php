<?php

use Illuminate\Database\Seeder;

class CornflakesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('cornflakes')->insert([
            'naam' => 'Lege compartment',
            'merk' => 'null',
            'compartment' => 'links',
        ]);

        DB::table('cornflakes')->insert([
            'naam' => 'Lege compartment',
            'merk' => 'null',
            'compartment' => 'rechts',
        ]);

    }
}