<?php

use Illuminate\Database\Seeder;

class CompartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('compartment')->insert([
            'compartment' => 'links',
            'vol' => '0',
        ]);

        DB::table('compartment')->insert([
            'compartment' => 'rechts',
            'vol' => '0',
        ]);
    }
}