<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => 'Tijs',
          'email' => 'TijsRuigrok15@gmail.com',
          'password' => bcrypt('123123')
      ]);
      
      DB::table('users')->insert([
        'name' => 'Tim',
        'email' => 'timesveldt@hotmail.com',
        'password' => bcrypt('testtest')
      ]);
      DB::table('users')->insert([
        'name' => 'Vyash',
        'email' => 'x@x.x',
        'password' => bcrypt('x')
      ]);
    }
}