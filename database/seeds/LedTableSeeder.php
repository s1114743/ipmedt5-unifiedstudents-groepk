<?php

use Illuminate\Database\Seeder;

class LedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('led')->insert([
            'led_on' => 'null',
        ]);
    }
}
