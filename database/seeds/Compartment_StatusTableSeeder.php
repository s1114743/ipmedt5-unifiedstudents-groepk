<?php

use Illuminate\Database\Seeder;

class Compartment_StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('compartment_status')->insert([
            'compartment' => 'links',
            'vol' => '0',
        ]);

        DB::table('compartment_status')->insert([
            'compartment' => 'rechts',
            'vol' => '0',
        ]);
    }
}