<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compartment extends Model
{
    public $timestamps = false;
    protected $table = "compartment";
    protected $fillable =
    [
        'compartment',
        'vol',
    ];
}