<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cornflakes;
use App\Users;
use App\Compartment;
use App\QueueHistory;
use Auth;
Use DB;

class PageController extends Controller
{
    //
    public function logout() {
        return view('myflakes.logout');
    }

    public function profiles() {
        return view('myflakes.profiles');
    }

    public function create() {
        return view('myflakes.add')->with('soorten', Cornflakes::all());
    }

    public function status() {
        return view('myflakes.status');
    }

    public function account() {
        $currentUser = Auth::user()->name;
        $currentUserQueueHistory = QueueHistory::where('name', $currentUser);
        return view('myflakes.account')->with('history', QueueHistory::where('name', $currentUser)->get());
        
    }
    
    public function dev() {
        return view('myflakes.dev');
    }

    public function store(Request $request) {
        $cornflakes = new Cornflakes();
        $compartment = new Compartment();

        $compartmentLinks = Compartment::find(1);
        $compartmentRechts = Compartment::find(2);


        $cornflakeLinks = Cornflakes::find(1);
        $cornflakeRechts = Cornflakes::find(2);

        if ($request->input('compartment') == 'links') {
            Cornflakes::where('id', '1')->update(['naam' => $request->input('naam'), 'merk' => $request->input('merk'), 'compartment' => $request->input('compartment')]);
            $compartmentLinks->vol = 1;
            $compartmentLinks->save();
        } else {
            Cornflakes::where('id', '2')->update(['naam' => $request->input('naam'), 'merk' => $request->input('merk'), 'compartment' => $request->input('compartment')]);
            $compartmentRechts->vol = 1;
            $compartmentRechts->save();
        }
    
        try {
            $cornflakes->save();
            return redirect('/dashboard');
        }
        catch (Exception $e){
            return redirect('/dashboard');
        }
    }

    public function destroy(Request $request, $id) {
        $compartmentLinks = Compartment::find(1);
        $compartmentRechts = Compartment::find(2);
        $updateLinks = ['naam' => 'Lege compartment', 'merk' => 'null', 'compartment' => 'links'];
        $updateRechts = ['naam' => 'Lege compartment', 'merk' => 'null', 'compartment' => 'rechts'];


        if ($id == 1) {
            Cornflakes::where('id', $id)->update($updateLinks);
            $compartmentLinks->vol = 0;
            $compartmentLinks->save();
        } else {
            Cornflakes::where('id', $id)->update($updateRechts);
            $compartmentRechts->vol = 0;
            $compartmentRechts->save();
        }
        return redirect('/dashboard');
    }

    public function update(Request $request) {
        $updateLinks = ['naam' => $request->input('naam-links'), 'merk' => $request->input('merk-links'), 'compartment' => 'links'];
        $updateRechts = ['naam' => $request->input('naam-rechts'), 'merk' => $request->input('merk-rechts'), 'compartment' => 'rechts'];

        Cornflakes::where('id', 1)->update($updateLinks);
        Cornflakes::where('id', 2)->update($updateRechts);

        $cornflakes = new Cornflakes();
        
        return redirect('/dashboard')->with('status', 'Cornflake aangepast!');

    }
    
}