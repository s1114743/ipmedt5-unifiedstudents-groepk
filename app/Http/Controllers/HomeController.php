<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dashboard;
use App\Cornflakes;
use App\Compartment;
use App\Compartment_Status;
use App\Queue;
use App\Status;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $currentUser = Auth::user()->name;
        $compartments = DB::table('compartment')->get();
        $compartmentLinksStatus = $compartments->first()->vol;
        $compartmentRechtsStatus = $compartments->last()->vol;


        $data = [
            'cornflakes' => Cornflakes::all(),
            'compartmentLinks' => Cornflakes::where('compartment', 'links')->first(),
            'compartmentRechts' => Cornflakes::where('compartment', 'rechts')->first(),
            'disableLinks' => $compartmentLinksStatus,
            'disableRechts' => $compartmentRechtsStatus,
            'queue' => Queue::where('name', $currentUser)->get(),
            'status' => Status::all()->first(),
            ];

        return view('dashboard')->with('data', $data);

    }

    public function store(Request $request) {
        $dashboard = new Dashboard();
        $dashboard->datum = $request->input('datum');
        $dashboard->tijdstip = $request->input('tijdstip');

        try {
            $dashboard->save();
            return redirect('/dashboard');
        }
        catch (Exception $e){
            return redirect('/');
        }
    }

    public function status() {
        return Status::all();
    }
}