<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Led;

class LedController extends Controller
{
    public function aanuit(){
        $led = Led::all()->first();
  
        if ($led->led_on == 'uit'){
          $led->led_on = 'aan';
        }
        else{
          $led->led_on = 'uit';
        }
        $led->save();
        return redirect('/dev');
      }
    
    public function cornflakeLinks(){
      $led = Led::all()->first();
      
      $led->led_on = 'links';

      $led->save();
      return redirect('/dev');

    }

    public function cornflakeRechts(){
      $led = Led::all()->first();
  
      $led->led_on = 'rechts';

      $led->save();
      return redirect('/dev');

    }
}