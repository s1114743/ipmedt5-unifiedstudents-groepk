<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Queue;
use App\Cornflakes;
use Auth;

class PlanningsController extends Controller
{
    //
    public function execute() {
        $currentDate = date("d/m/Y");
        $currentTime = date("h:i:s");
        $planningDate = "Select datum from dashboard";
    }

    public function store(Request $request) {
        $cornflake = Cornflakes::where('id', $request->cornflake)->first();
        $queue = new Queue();
        $dateTime = $request->tijdstip . " " . $request->datum;
        $queue->cornflake = $cornflake->naam;
        $queue->compartment = $cornflake->compartment;
        $queue->name = Auth::user()->name;
        $queue->time = $dateTime;
        $queue->save();
        return redirect('/dashboard')->with('status', 'Ontbijt staat ingepland voor: '.$dateTime);
    }

    public function destroy($id) {
        Queue::where('id', $id)->delete();
        return redirect('/dashboard')->with('status', 'Queue verwijderd');
    }
}