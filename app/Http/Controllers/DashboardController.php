<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dashboard;
use App\Cornflakes;

class DashboardController extends Controller
{
    // 
    public function index() {
        return view('myflakes.dashboard');
    }
}
