<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Led extends Model
{
    protected $fillable = ['led_on'];
    public $primary_key = ['led_on'];
    protected $table = 'led';
}