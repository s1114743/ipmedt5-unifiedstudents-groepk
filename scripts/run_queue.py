import mysql.connector
from datetime import datetime
import time


def moveToQueueHistory(data):
    insert_into_history = (
        "INSERT INTO queue_history (cornflake, compartment, name, time) VALUES (%s, %s, %s, %s)")
    remove_from_queue = (
        "DELETE FROM queue WHERE time = %s")
    date_to_delete = (data[3],)
    # print((data[3],))
    mycursor.execute(insert_into_history, data)
    mycursor.execute(remove_from_queue, date_to_delete)

    mydb.commit()


def calculateTimeDifference():
    t1 = datetime.strptime(current_time, date_fmt)
    t2 = datetime.strptime(row[4], date_fmt)
    dts1 = time.mktime(t1.timetuple())
    dts2 = time.mktime(t2.timetuple())

    return(int(dts1-dts2) / 60)


def update_led_direction(data):
    update_led_on = (
        "UPDATE led SET led_on=%s WHERE id='1'")
    mycursor.execute(update_led_on, data)
    mydb.commit()


if __name__ == '__main__':

    date_fmt = "%H:%M %Y-%m-%d"

    while True:
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            passwd="root",
            database="myflakes"
        )
        current_time = datetime.now().strftime(date_fmt)

        print("\n")
        mycursor = mydb.cursor()
        mycursor.execute("SELECT * FROM queue")

        queue_table = mycursor.fetchall()
        print('current time: ' + current_time)

        for row in queue_table:
            print(calculateTimeDifference())
            if row[4] == current_time:
                print(row[4] + " now")
                moveToQueueHistory((row[1], row[2], row[3], row[4],))
                update_led_direction((row[1],))
            else:
                print(row[4] + " not yet")

        time.sleep(5)

        # Hier moeten nog komen dat je de waarde van de led verandert naar links of rechts
